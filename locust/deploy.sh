kubectl create ns locust
kubectl create configmap loadtest-locustfile --from-file locustfile.py -n locust

helm repo add deliveryhero https://charts.deliveryhero.io/
helm install locust deliveryhero/locust --version 0.31.5\
  --set loadtest.locust_host="http://front-end.sock-shop.svc.cluster.local:80" \
  --set loadtest.locust_locustfile="locustfile.py" \
  --set loadtest.locust_locustfile_configmap="loadtest-locustfile" --create-namespace -n locust \
  --set worker.replicas=2
