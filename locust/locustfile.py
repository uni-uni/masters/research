import base64
import random

from locust import HttpUser, task

def b64e(s):
    return base64.b64encode(s.encode()).decode()

class WebTasks(HttpUser):

    @task(1)
    def load(self):
        base64string = b64e("user:password").replace('\n', '')

        catalogue = self.client.get("/catalogue").json()
        category_item = random.choice(catalogue)
        item_id = category_item["id"]

        self.client.get("/")
        self.client.get("/login", headers={"Authorization":"Basic %s" % base64string})
        self.client.get("/category.html")
        self.client.get("/detail.html?id={}".format(item_id))
        self.client.delete("/cart")
        self.client.post("/cart", json={"id": item_id, "quantity": 1})
        self.client.get("/basket.html")
        self.client.post("/orders")
